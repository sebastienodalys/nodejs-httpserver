// Configuration de base
const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');

const app = express();

app.use(express.static('public'));

// Handlebars middleware
app.engine('handlebars', exphbs.engine({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

const members = [
    {
        id: 1,
        name: "John",
        email: "john@gmail.com",
        status: "active",
    },
    {
        id: 2,
        name: "Jane",
        email: "jane@gmail.com",
        status: "inactive",
    },
    {
        id: 3,
        name: "Joe",
        email: "joe@gmail.com",
        status: "active",
    }
];

// Requête GET
app.get('/', (request, response) => {
    response.render('index', {
        title: 'Nos membres',
        members
    });
});
app.get('/api/members', (request, response) => {
    response.json(members);
});
app.get('/api/members/:id', (request, response) => {
    const foundedMember = members.some(member => member.id === parseInt(request.params.id));
    if (foundedMember) {
        response.json(members.filter(member => member.id === parseInt(request.params.id)));
    }
    else {
        response.status(400).json({msg: `Pas de membre trouvé avec l'identifiant ${request.params.id}`});
    }
});

app.use(express.static(path.join(__dirname, 'public')));

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Le serveur à démarré sur le port ${PORT}`));